import java.util.Arrays;
import java.util.Random;

public class funcs {
    static char[] makeAlphaBet() {
        char[] nn = new char[26];

        for (int i = 0; i < nn.length; i++) {
            nn[i] = (char) (i + 65);
        }
        return nn;
    }

    static int summAfterOne(int[] nn) {
        boolean flag = false;
        int sum = 0;
        for (int i = 0; i < nn.length; i++) {
            if (flag)
                sum += nn[i];
            else if (nn[i] == 1)
                flag = true;
        }
        return sum;
    }


    static int findMinMainDet(int[][] nn) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < nn.length; i++) {
            if (min > nn[i][i])
                min = nn[i][i];
        }
        return min;
    }

    static int sumMainDet(int[][] nn) {
        int sum = 0;
        for (int i = 0; i < nn.length; i++) {
            sum += nn[i][i];
        }
        return sum;
    }

    static int sumSideDet(int[][] nn) {
        int sum = 0;
        for (int i = 0; i < nn.length; i++) {
            sum += nn[i][nn.length - i - 1];
        }
        return sum;
    }

    static int findMax(int[] nn) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nn.length; i++)
            if (max < nn[i])
                max = nn[i];
        return max;
    }

    static int findMax(int[][] nn) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nn.length; i++)
            for (int j = 0; j < nn[0].length; j++) {
                if (max < nn[i][j])
                    max = nn[i][j];
            }
        return max;
    }

    static int findMax(int[][][] nnn) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nnn.length; i++)
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    if (max < nnn[i][j][k])
                        max = nnn[i][j][k];
                }
            }
        return max;
    }

    static int findMaxElArr(int[][][] nnn) {
        int max = Integer.MIN_VALUE;
        int max_i = 0;
        for (int i = 0; i < nnn.length; i++) {
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    if (max < nnn[i][j][k]) {
                        max = nnn[i][j][k];
                        max_i = i;
                    }
                }
            }
        }
        return max_i;
    }

    static void eqOnTwo(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                if (Math.abs(nn[i][j]) > 10)
                    nn[i][j] /= 2;
            }
        }
    }

    static void changeSign(int[][][] nnn) {
        for (int i = 0; i < nnn.length; i++) {
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    nnn[i][j][k] *= -1;
                }
            }
        }
    }

    static double findMax(double[][] nn) {
        double max = Double.MIN_VALUE;
        for (int i = 0; i < nn.length; i++)
            for (int j = 0; j < nn[0].length; j++) {
                if (max < Math.abs(nn[i][j]))
                    max = Math.abs(nn[i][j]);
            }
        return max;
    }

    static int findMin(int[] nn) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < nn.length; i++)
            if (min > nn[i])
                min = nn[i];
        return min;
    }

    static void printArray(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void printArray(double[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void sortRevArr(int[][] x) {
        int arrl = x.length * x[0].length, tmp;
        for (int i = 0; i < arrl; i++) {
            for (int j = 0; j < x.length; j++) {
                for (int k = 0; k < x[0].length - 1; k++) {
                    if (x[j][k + 1] > x[j][k]) {
                        tmp = x[j][k + 1];
                        x[j][k + 1] = x[j][k];
                        x[j][k] = tmp;
                    }
                }
                if (j != x.length - 1 && x[j + 1][0] > x[j][x[0].length - 1]) {
                    tmp = x[j][x[0].length - 1];
                    x[j][x[0].length - 1] = x[j + 1][0];
                    x[j + 1][0] = tmp;
                }
            }
        }
    }

    static void sortRevArr(int[] x) {
        int max, max_i;
        for (int i = 0; i < x.length; i++) {
            max = x[i];
            max_i = i;
            for (int j = i + 1; j < x.length; j++) {
                if (max < x[j]) {
                    max = x[j];
                    max_i = j;
                }
            }
            if (max_i != i) {
                x[max_i] = x[i];
                x[i] = max;
            }
        }
    }


    static void sort2dArr(int[][] x) {
        int arrl = x.length * x[0].length, tmp;
        for (int a = 0; a < arrl; a++) {
            for (int i = 0; i < x.length; i++) {
                for (int j = 0; j < x[0].length - 1; j++) {
                    if (x[i][j] > x[i][j + 1]) {
                        tmp = x[i][j + 1];
                        x[i][j + 1] = x[i][j];
                        x[i][j] = tmp;
                    }
                }
                if (i != (x.length - 1) && x[i + 1][0] < x[i][x[0].length - 1]) {
                    tmp = x[i + 1][0];
                    x[i + 1][0] = x[i][x[0].length - 1];
                    x[i][x[0].length - 1] = tmp;
                }
            }
        }
    }

    static void sort3dArr(int[][][] x) {
        int arrl = x.length * x[0].length * x[0][0].length, tmp;
        for (int l = 0; l < arrl; l++) {
            for (int i = 0; i < x.length; i++) {
                for (int j = 0; j < x[0].length; j++) {
                    for (int k = 0; k < x[0][0].length - 1; k++) {
                        if (x[i][j][k] > x[i][j][k + 1]) {
                            tmp = x[i][j][k + 1];
                            x[i][j][k + 1] = x[i][j][k];
                            x[i][j][k] = tmp;
                        }
                        if (j != x[0].length - 1 && x[i][j][x[0][0].length - 1] > x[i][j + 1][0]) {
                            tmp = x[i][j + 1][0];
                            x[i][j + 1][0] = x[i][j][x[0][0].length - 1];
                            x[i][j][x[0][0].length - 1] = tmp;
                        }
                    }
                }
                if (i != x.length - 1 && x[i][x[0].length - 1][x[0][0].length - 1] > x[i + 1][0][0]) {
                    tmp = x[i][x[0].length - 1][x[0][0].length - 1];
                    x[i][x[0].length - 1][x[0][0].length - 1] = x[i + 1][0][0];
                    x[i + 1][0][0] = tmp;
                }
            }
        }
    }

    static void sortArr(int[] x) {
        int min, min_i;
        for (int i = 0; i < x.length; i++) {
            min = x[i];
            min_i = i;
            for (int j = i + 1; j < x.length; j++) {
                if (min > x[j]) {
                    min = x[j];
                    min_i = j;
                }
            }
            if (min_i != i) {
                x[min_i] = x[i];
                x[i] = min;
            }
        }
    }

    static int[][] make2dArray(int a) {
        Random random = new Random();
        int[][] aa = new int[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                aa[i][j] = random.nextInt(99);
            }
        }
        return aa;
    }

    static double[][] make2dArrayDouble(int a) {
        Random random = new Random();
        double[][] aa = new double[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                aa[i][j] = random.nextInt(25) + 10;
            }
        }
        return aa;
    }

    static int[] makeArray(int n) {
        Random random = new Random();
        int[] x = new int[n];
        for (int i = 0; i < n; i++) {
            x[i] = random.nextInt(98) + 1;
            x[i] *= Math.pow(-1, random.nextInt(99));
        }
        return x;
    }

    static double[] makeArrayDouble(int n) {
        Random random = new Random();
        double[] x = new double[n];
        for (int i = 0; i < n; i++) {
            x[i] = random.nextDouble() + 1;
            x[i] *= Math.pow(-1, random.nextInt(99));
        }
        return x;
    }

    static int srArifm(int[][] nn) {
        int srArifm, sum = 0;
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                sum += nn[i][j];
            }
        }
        srArifm = sum / (nn.length * nn[0].length);
        return srArifm;
    }

    static int srArifm(int[][][] nnn) {
        int srArifm, sum = 0;
        for (int i = 0; i < nnn.length; i++) {
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    if (nnn[i][k][j] > 0)
                        sum += nnn[i][j][k];
                }
            }
        }
        srArifm = sum / (nnn.length * nnn[0].length * nnn[0][0].length);
        return srArifm;
    }

    static int colsSum(int[][] x, int a) {
        int sum = 0;
        for (int j = 0; j < x[0].length; j++)
            sum += x[j][a];
        return sum;
    }

    static void changeCols(int[][] x, int a, int b) {
        int tmp;
        for (int i = 0; i < x.length; i++) {
            tmp = x[i][a];
            x[i][a] = x[i][b];
            x[i][b] = tmp;
        }
    }

    static void changeRows(int[][] x, int a, int b) {
        int tmp;
        for (int i = 0; i < x.length; i++) {
            tmp = x[a][i];
            x[a][i] = x[b][i];
            x[b][i] = tmp;
        }
    }

    static void sortArrCols(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (colsSum(x, i) > colsSum(x, j)) {
                    changeCols(x, i, j);
                }
            }
        }
    }

    static void sortRevArrCols(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (colsSum(x, i) < colsSum(x, j)) {
                    changeCols(x, i, j);
                }
            }
        }
    }

    static void sortArrRows(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (rowsSum(x, i) > rowsSum(x, j)) {
                    changeRows(x, i, j);
                }
            }
        }
    }

    static void sortRevArrRows(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (rowsSum(x, i) < rowsSum(x, j)) {
                    changeRows(x, i, j);
                }
            }
        }
    }

    static int[][] arrayCopy(int[][] x) {
        int a = x.length;
        int b = x[0].length;
        int[][] nn = new int[a][b];

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                nn[i][j] = x[i][j];
            }
        }
        return nn;
    }

    static int[] arrayCopy(int[] x) {
        int a = x.length;
        int[] nn = new int[a];

        for (int i = 0; i < a; i++) {
            nn[i] = x[i];
        }
        return nn;
    }

    static int rowsSum(int[][] x, int a) {
        int sum = 0;
        for (int i = 0; i < x.length; i++)
            if (isEven(x[a][i]))
                sum += x[a][i];
        return sum;
    }

    static boolean isEven(int x) {
        if (x % 2 == 0)
            return true;
        else
            return false;
    }

    static void printArrayColsSum(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        for (int k = 0; k < nn.length; k++) {
            System.out.print(funcs.colsSum(nn, k) + "\t");
        }
        System.out.println();
    }

    static void printArrayRowsSum(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.print("| " + funcs.rowsSum(nn, i));
            System.out.println();
        }
    }

    static boolean isPrime(int x) {
        boolean flag = true;
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    static int primeRowsCount(int[][] x) {
        int count = 0;
        for (int i = 0; i < x.length; i++)
            for (int j = 0; j < x[0].length; j++)
                if (isPrime(x[i][j])) {
                    count++;
                    break;
                }
        return count;
    }

    static int primeColsCount(int[][] x) {
        int count = 0;
        for (int i = 0; i < x.length; i++)
            for (int j = 0; j < x[0].length; j++)
                if (isPrime(x[j][i])) {
                    count++;
                    break;
                }
        return count;
    }

    static String word(int a, String[] words) {
        int word = 0;
        int n = a;
        if (n > 99)
            n = toTwo(n);

        if (n >= 21 && n <= 99)
            n = toOne(n);

        if (n >= 10 && n <= 20 || n >= 5 || n == 0)
            word = 2;
        else if (n >= 2 && n <= 4)
            word = 1;

        return a + " " + words[word];
    }

    static int toTwo(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 2));
        return a;
    }

    static int toOne(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 1));
        return a;
    }

    static int[][] Arrays2dMult(int[][] nn, int[][] mm) {
        int[][] nm = new int[nn.length][mm[0].length];
        if (nn[0].length == mm.length)
            for (int i = 0; i < nn[0].length; i++) {
                for (int j = 0; j < mm.length; j++) {
                    int tmp = 0;
                    for (int k = 0; k < mm.length; k++) {
                        tmp += nn[i][k] * mm[k][j];
                    }
                    nm[i][j] = tmp;
                }
            }
        else
            System.out.println("Эти матрицы нельзя умножить, тк кол-во столбцов матрицы A отличается от кол-ва строк матрицы B.");
        return nm;
    }

    static int[][][] fillArray3Whiles(int xx) {
        int[][][] nnn = new int[xx][xx][xx];
        int
                a = 0, b = 0, c = 0;
        while (a < xx) {
            while (b < xx) {
                while (c < xx) {
                    nnn[a][b][c] = 1;
                    c++;
                }
                c = 0;
                b++;
            }
            b = 0;
            a++;
        }


        return nnn;
    }

    static int[][][] make3dArray(int xx) {
        Random rnd = new Random();
        int[][][] nnn = new int[xx][xx][xx];
        for (int i = 0; i < nnn.length; i++) {
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    nnn[i][j][k] = rnd.nextInt(98) + 1;
                    nnn[i][j][k] *= Math.pow(-1, k);
                }
            }
        }
        return nnn;
    }

    static int sum3d(int[][][] nnn) {
        int sum = 0;
        for (int i = 0; i < nnn.length; i++) {
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    sum += nnn[i][j][k];
                }
            }
        }
        return sum;
    }

    static void printArray(int[][][] nnn) {
        for (int i = 0; i < nnn.length; i++) {
            System.out.println("{");
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    System.out.print(nnn[i][j][k] + "\t");
                }
                System.out.println();
            }
            System.out.println("}\n");
        }
    }

    static boolean isSame(int[] nn) {
        int x = nn[0];
        boolean flag = true;
        for (int i = 0; i < nn.length; i++) {
            if (nn[i] != x) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    static int[] fillSameArray(int a, int b) {
        int[] nn = new int[a];
        for (int i = 0; i < a; i++) {
            nn[i] = b;
        }
        return nn;
    }

    static String printArray(int[] nn) {
        String s = "";
        for (int i = 0; i < nn.length; i++) {
            if (i != nn.length - 1)
                s += nn[i] + ", ";
            else
                s += nn[i] + ".";
        }
        return s;
    }

    static String printArray(char[] nn) {
        String s = "";
        for (int i = 0; i < nn.length; i++) {
            if (i != nn.length - 1)
                s += nn[i] + ", ";
            else
                s += nn[i] + ".";
        }
        return s;
    }

    static String printArray(double[] nn) {
        String s = "";
        for (int i = 0; i < nn.length; i++) {
            if (i != nn.length - 1)
                s += nn[i] + ", ";
            else
                s += nn[i] + ".";
        }
        return s;
    }

    static void printXY(int[][][] nnn) {
        for (int i = 0; i < nnn.length; i++) {
            System.out.print("\nz = " + i + ": ");
            for (int j = 0; j < nnn[0].length; j++) {
                for (int k = 0; k < nnn[0][0].length; k++) {
                    if (k == nnn[0][0].length - 1 && j == nnn[0].length - 1)
                        System.out.print(nnn[i][j][k] + ";");
                    else
                        System.out.print(nnn[i][j][k] + ", ");
                }
            }
        }
    }

    static int sumPositive(int[] nn) {
        int sum = 0;
        for (int i = 0; i < nn.length; i++) {
            if (nn[i] > 0)
                sum += nn[i];
        }
        return sum;
    }

    static boolean isAsc(int[] nn) {
        boolean flag = true;
        for (int i = 0; i < nn.length; i++) {
            for (int j = i + 1; j < nn.length; j++) {
                if (nn[j] < nn[i]) {
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }

    static int[] findMaxElements(int[] nn, int a) {
        int[] aa = new int[a];
        int k = 0;
        sortRevArr(nn);
        for (int i = 0; i < nn.length; i++) {
            if (k < a) {
                boolean flag = true;
                for (int j = 0; j < a; j++) {
                    if (nn[i] == aa[j]) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    aa[k] = nn[i];
                    k++;
                }
            }
        }
        return aa;
    }

    static int[] reverseArray(int[] nn) {
        int[] gg = new int[nn.length];
        for (int i = 0; i < nn.length; i++) {
            gg[i] = nn[nn.length - 1 - i];
        }
        return gg;
    }

    static int[] stepArray(int[] nn, int step) {
        int[] mm = arrayCopy(nn);
        for (int k = 0; k < step; k++) {
            int[] gg = arrayCopy(mm);
            for (int i = 0; i < mm.length; i++)
                if (i + 1 != mm.length)
                    mm[i + 1] = gg[i];
                else
                    mm[0] = gg[gg.length - 1];
        }
        return mm;
    }

    static boolean isDiff(int[] nn) {
        int[] mm = arrayCopy(nn);
        boolean flag = true;
        for (int i = 0; i < nn.length; i++) {
            for (int j = i + 1; j < mm.length; j++) {
                if (nn[i] == mm[j]) {
                    flag = false;
                    break;
                }
            }
        }

        return flag;
    }

    static int howMuchDiffs(int[] nn) {
        int[] mm = arrayCopy(nn);
        int count = 0;
        for (int i = 0; i < nn.length; i++) {
            boolean flag = true;
            for (int j = i + 1; j < mm.length; j++) {
                if (nn[i] == mm[j]) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                count++;
        }

        return count;
    }

    static int countZeros(int[] nn) {
        int count = 0;
        for (int i = 0; i < nn.length; i++) {
            if (nn[i] == 0)
                count++;
        }
        return count;
    }

    static int findLessK(int[] nn, int k) {
        int count = 0;

        for (int i = 0; i < nn.length; i++) {
            if (nn[i] < k)
                count++;
        }
        return count;
    }

    static int sum2d(int[][] nn) {
        int sum = 0;
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                sum += nn[i][j];
            }
        }
        return sum;
    }

    static int[][] maxXY(int[][][] nnn) {
        int[][] nn = new int[nnn[0].length][nnn[0][0].length];
        if (findMax(nnn[0]) > findMax(nnn[1]))
            nn = arrayCopy(nnn[0]);
        else
            nn = arrayCopy(nnn[1]);

        return nn;
    }

    static void norm(double[] nn) {
        int sum = 0;
        for (int i = 0; i < nn.length; i++) {
            if (nn[i] >= 0)
                sum += nn[i];
        }

        for (int i = 0; i < nn.length; i++) {
            nn[i] /= sum;
        }

    }

    static void normMax(double[][] nn) {
        double max = Double.MIN_VALUE;

        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                if (max < nn[i][j])
                    max = nn[i][j];
            }
        }

        for (int i = 0; i < nn.length; i++)
            for (int j = 0; j < nn[0].length; j++)
                nn[i][j] /= max;
    }

    static void sortXYArr(int[][][] nnn) {
        int[][] nn = new int[nnn[0].length][nnn[0][0].length];

        System.out.println("Сумма первого массива: " + sum2d(nnn[0]) + "\n" + "Сумма второго массива: " + sum2d(nnn[1]) + "\n" + "Сумма третьего массива: " + sum2d(nnn[2]));
        for (int a = 0; a < nnn.length; a++) {
            for (int b = a + 1; b < nnn.length; b++) {
                if (sum2d(nnn[a]) > sum2d(nnn[b])) {
                    System.out.println("");
                    for (int i = 0; i < nn.length; i++) {
                        for (int j = 0; j < nn[0].length; j++) {
                            nn[i][j] = nnn[a][i][j];
                        }
                    }
                    for (int i = 0; i < nn.length; i++) {
                        for (int j = 0; j < nn[0].length; j++) {
                            nnn[a][i][j] = nnn[b][i][j];
                            nnn[b][i][j] = nn[i][j];
                        }
                    }
                }
            }
        }
    }

    static void sortXYArrSrArifm(int[][][] nnn) {
        int[][] nn = new int[nnn[0].length][nnn[0][0].length];

        System.out.println("Среднее арифметическое первого массива: " + srArifm(nnn[0]) + "\n" + "Среднее арифметическое второго массива: " + srArifm(nnn[1]) + "\n" + "Среднее арифметическое третьего массива: " + srArifm(nnn[2]));
        for (int a = 0; a < nnn.length; a++) {
            for (int b = a + 1; b < nnn.length; b++) {
                if (srArifm(nnn[a]) > srArifm(nnn[b])) {
                    System.out.println("");
                    for (int i = 0; i < nn.length; i++) {
                        for (int j = 0; j < nn[0].length; j++) {
                            nn[i][j] = nnn[a][i][j];
                        }
                    }
                    for (int i = 0; i < nn.length; i++) {
                        for (int j = 0; j < nn[0].length; j++) {
                            nnn[a][i][j] = nnn[b][i][j];
                            nnn[b][i][j] = nn[i][j];
                        }
                    }
                }
            }
        }
    }
}


